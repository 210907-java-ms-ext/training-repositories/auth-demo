/*
    GET http://localhost:8082/auth-demo/users

    If we were requesting the users via ajax, we would:
        - create a new XMLHttpRequest
        - invoke the open method with METHOD & URL
        - define onreadystatechange callback (what to do when the response is done)
                - if we get a 200 and some data:
                    - parse JSON to an array of JS objects 
                    - add object data to the dom
        - obtain the token from sessionStorage and set the Authorization request header 
        - send request
*/

/*
    // these functions are equivalent
    function printResponse(resp){
        console.log(resp);
    }

    let printResponse2 = (resp) => console.log(resp);
*/

let displayUsers = users => {    
    const mainContent = document.getElementById("main-content");
    const userList = document.createElement("ul");
    for(let user of users){
        const userListItem = document.createElement("li");
        userListItem.innerText = `${user.username} ( ${user.userRole.toLowerCase()} )`;
        userList.appendChild(userListItem);
    }
    mainContent.appendChild(userList);
}

const token = sessionStorage.getItem("token");

/*
"falsy" - null, undefined, "", 0, false, NaN
"truthy" - anything else
*/

if(token){ // string is type coerced to a boolean

    fetch("http://localhost:8082/auth-demo/users", {headers: {"Authorization" : token}})
    .then(response => response.json()) //converts JSON response body into JS objects 
    .then(displayUsers);

/*
    - fetch returns a promise of a response
    - when the promise is resolved, we are able to perform a
        callback on the response
    - in this case, we want to obtain the response body and parse it into JS objects
    - the response.json() also returns a promise itself, one of the parsed response
    - we are then able to register a callback to process the parsed response body (displayUsers)
*/
} else {
    window.location.href = "http://localhost:8082/auth-demo/static/login.html"
}




