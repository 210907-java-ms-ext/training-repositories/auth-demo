package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.models.User;
import com.revature.models.UserRole;
import com.revature.services.AuthService;
import com.revature.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UserServlet extends HttpServlet {

    private AuthService authService = new AuthService();
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // look at token - check if the appropriate role is present
        String authToken = req.getHeader("Authorization");

        boolean tokenIsValidFormat = authService.validateToken(authToken);
        if(!tokenIsValidFormat){
            resp.sendError(400, "Improper token format; cannot fulfill your request");
        } else {
            User currentUser = authService.findUserByToken(authToken); // return null if none found

            if(currentUser==null){
                resp.sendError(401, "Auth token invalid - no user");
            } else {
                // if there is a valid token and that token is for an admin, return list of users
                if(currentUser.getUserRole() == UserRole.ADMIN){
                    resp.setStatus(200);
                    try(PrintWriter pw = resp.getWriter();){
                        // write to response body
                        List<User> users = userService.getAllUsers();
                        ObjectMapper om = new ObjectMapper(); // ObjectMapper utilizes Java Reflection to introspect
                        // on the user object to create my json
                        // { "id" : 1, "username": "sallyjenkins", "password" : "pass123", "userRole" : "GENERAL"}
                        String userJson = om.writeValueAsString(users);
                        pw.write(userJson);
                    }
                } else {
                    // if there is a valid token, but it's that of a general user rather than an admin, we could send back a 403
                    resp.sendError(403, "Invalid user role for current request");
                }
            }


        }

    }



}
