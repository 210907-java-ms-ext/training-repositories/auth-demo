package com.revature.services;

import com.revature.daos.UserDao;
import com.revature.daos.UserDaoImpl;
import com.revature.models.User;

import java.util.List;

public class UserService {

    private UserDao userDao = new UserDaoImpl();

    public List<User> getAllUsers(){
        return userDao.getAll();
    }

    public User getUserByCredentials(String username, String password){
        if(username==null || username.isEmpty() || password == null || password.isEmpty()){
            return null;
        }
        return userDao.getUserByUsernameAndPassword(username, password);
    }

    public User getUserById(int id){
        return userDao.getUserById(id);
    }

}
